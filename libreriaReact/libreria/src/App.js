import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import ListaAutores from "./components/Autor/ListaAutores";
import CrearAutor from "./components/Autor/CrearAutor";
import EditarAutor from "./components/Autor/EditarAutor";
import CrearEditorial from "./components/Editorial/CrearEditorial";
import ListaEditoriales from "./components/Editorial/ListaEditoriales";
import "./App.css";
import EditarEditorial from "./components/Editorial/EditarAutor";
import ListaLibros from "./components/Libreria/ListaLibros";
import CrearLibro from "./components/Libreria/CrearLibro";

function App() {
  return (
    <Router>
      <div className="container">
        <nav class="navbar navbar-expand-lg navbar-light navbar-mio">
          <div class="container-fluid">
            <button
              class="navbar-toggler"
              type="button"
              data-bs-toggle="collapse"
              data-bs-target="#navbarNav"
              aria-controls="navbarNav"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
              <ul class="navbar-nav">
                <Link to="/listaLibros">
                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#">
                      Libros
                    </a>
                  </li>
                </Link>

                <Link to="/listaAutores">
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      Autores
                    </a>
                  </li>
                </Link>
                <Link to="/listaEditoriales">
                  <li class="nav-item">
                    <a class="nav-link" href="#">
                      Editoriales
                    </a>
                  </li>
                </Link>
              </ul>
            </div>
          </div>
        </nav>
      </div>
      <Switch>
        <Route path="/editarAutor/:autorEditar" component={EditarAutor} />
        <Route path="/listaAutores" component={ListaAutores} />
        <Route path="/crearAutor" component={CrearAutor} />
        <Route path="/listaEditoriales" component={ListaEditoriales} />
        <Route path="/crearEditorial" component={CrearEditorial} />
        <Route
          path="/editarEditorial/:editorialEditar"
          component={EditarEditorial}
        />
        <Route path="/listaLibros" component={ListaLibros} />
        <Route path="/crearLibro" component={CrearLibro} />
      </Switch>
    </Router>
  );
}

export default App;
