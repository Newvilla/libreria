import React, { Fragment, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
const EditarEditorial = () => {
  const axios = require("axios");
  const Swal = require("sweetalert2");
  const { editorialEditar } = useParams();
  const editorialDatosEditar = JSON.parse(editorialEditar);
  let history = useHistory();
  const [editorial, setEditorial] = useState({
    nombre: editorialDatosEditar.nombre,
    alta: editorialDatosEditar.alta,
  });

  const handleChange = (e) => {
    setEditorial({
      ...editorial,
      [e.target.name]: e.target.value,
    });
  };
  const handleChecked = async (e) => {
    setEditorial({
      ...editorial,
      [e.target.name]: e.target.checked,
    });
  };

  const onSubmit = (e) => {
    e.preventDefault();

    Swal.fire({
      title: "¿Deseas Editar la Editorial?",
      showCancelButton: true,
      confirmButtonText: "Crear",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .put(
            `http://localhost:8080/editoriales/editar/${editorialDatosEditar.id}`,
            editorial
          )
          .then((res) => {
            if (res.status == 200) {
              Swal.fire("Editorial Editada con exito!", "", "success");
              setEditorial({ nombre: "", alta: false });
              history.push("/listaEditoriales");
            }
          });
      } else if (result.isDenied) {
      }
    });
  };
  return (
    <Fragment>
      <div className="container form-editar">
        <h1>Editar Editorial</h1>
        <form onSubmit={onSubmit}>
          <div class="mb-3">
            <label for="nombre" class="form-label">
              Nombre de la Editorial
            </label>
            <input
              type="text"
              class="form-control"
              id="nombre"
              name="nombre"
              aria-describedby="emailHelp"
              onChange={handleChange}
              defaultValue={editorialDatosEditar.nombre}
            />
          </div>

          <div class="mb-3 form-check">
            <input
              type="checkbox"
              class="form-check-input"
              id="alta"
              name="alta"
              onChange={handleChecked}
              defaultChecked={editorialDatosEditar.alta}
            />
            <label class="form-check-label" for="alta">
              Alta
            </label>
          </div>
          <button type="submit" class="btn btn-primary">
            Editar
          </button>
        </form>
      </div>
    </Fragment>
  );
};

export default EditarEditorial;
