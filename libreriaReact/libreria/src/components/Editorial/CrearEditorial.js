import React, { Fragment, useState } from "react";
import { useHistory } from "react-router-dom";
const CrearEditorial = () => {
  const axios = require("axios");
  const Swal = require("sweetalert2");
  let history = useHistory();
  const [editorial, setEditorial] = useState({
    nombre: "",
    alta: false,
  });
  const handleChange = (e) => {
    setEditorial({
      ...editorial,
      [e.target.name]: e.target.value,
    });
  };
  const handleChecked = async (e) => {
    setEditorial({
      ...editorial,
      [e.target.name]: e.target.checked,
    });
  };
  const onSubmit = (e) => {
    e.preventDefault();

    Swal.fire({
      title: "¿Deseas Crear la Editorial?",
      showCancelButton: true,
      confirmButtonText: "Crear",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post("http://localhost:8080/editoriales/crear/", editorial)
          .then((res) => {
            if (res.status == 201) {
              Swal.fire("Editorial creada con exito!", "", "success");
              setEditorial({ nombre: "", alta: false });
              history.push("/listaEditoriales");
            }
          });
      } else if (result.isDenied) {
      }
    });
  };
  const { nombre, alta } = editorial;
  return (
    <Fragment>
      <div className="container form-crear">
        <h1>Crear Editorial</h1>
        <form onSubmit={onSubmit}>
          <div class="mb-3">
            <label for="nombre" class="form-label">
              Nombre del la Editorial
            </label>
            <input
              type="text"
              class="form-control"
              id="nombre"
              name="nombre"
              aria-describedby="emailHelp"
              onChange={handleChange}
              value={nombre}
            />
          </div>

          <div class="mb-3 form-check">
            <input
              type="checkbox"
              class="form-check-input"
              id="alta"
              name="alta"
              onChange={handleChecked}
              checked={alta}
            />
            <label class="form-check-label" for="alta">
              Alta
            </label>
          </div>
          <button type="submit" class="btn btn-primary">
            Crear
          </button>
        </form>
      </div>
    </Fragment>
  );
};

export default CrearEditorial;
