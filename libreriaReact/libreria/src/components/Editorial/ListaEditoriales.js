import React, { Fragment, useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
const ListaEditoriales = () => {
  const axios = require("axios");
  const Swal = require("sweetalert2");
  const [editoriales, setEditoriales] = useState([]);

  const obtenerListaEditoriales = async () => {
    await axios
      .get("http://localhost:8080/editoriales/editoriales/")
      .then(function (response) {
        setEditoriales(response.data);
      });
  };
  const eliminarEditorial = (editorial) => {
    Swal.fire({
      title: "¿Seguro deseas eliminar esta Editorial?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .delete(`http://localhost:8080/editoriales/eliminar/${editorial.id}`)
          .then(function (response) {});
        Swal.fire("Eliminado!", "Editorial eliminada con exito.", "success");
        setEditoriales([]);
      }
    });
  };
  useEffect(() => {
    obtenerListaEditoriales();
  });
  return (
    <Fragment>
      <Fragment>
        <div className="container">
          <div className="header">
            <h1>Lista Editoriales</h1>
          </div>
          <div
            className="btn-toolbar"
            role="toolbar"
            aria-label="Toolbar with button groups"
          >
            <div class="btn-group" role="group" aria-label="Third group">
              <Link to="/CrearEditorial">
                <button className="btn btn-primary " type="button">
                  Crear Editorial
                </button>
              </Link>
            </div>
          </div>
          <table className="table">
            <thead className="table-dark">
              <tr>
                <th scope="col">Nombre</th>
                <th scope="col">Alta</th>
                <th></th>
                <th></th>
              </tr>
            </thead>
            <tbody>
              {editoriales.map((editorial) => (
                <tr>
                  <td>{editorial.nombre}</td>
                  <td>{editorial.alta ? "Alta" : "Baja"}</td>

                  <td>
                    <Link to={`/editarEditorial/${JSON.stringify(editorial)}`}>
                      <button type="button" className="btn btn-success">
                        Editar
                      </button>
                    </Link>
                  </td>
                  <td>
                    <button
                      type="button"
                      className="btn btn-danger"
                      onClick={() => eliminarEditorial(editorial)}
                    >
                      Eliminar
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </Fragment>
    </Fragment>
  );
};

export default ListaEditoriales;
