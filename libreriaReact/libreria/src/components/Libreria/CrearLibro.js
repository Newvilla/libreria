import React, { Fragment, useState, useEffect } from "react";
import { useHistory } from "react-router-dom";
const CrearLibro = () => {
  const axios = require("axios");
  const Swal = require("sweetalert2");
  let history = useHistory();
  const [libro, setLibro] = useState({
    isbn: null,
    titulo: "",
    anio: null,
    ejemplares: null,
    ejemplaresPrestados: null,
    ejemplaresRestantes: null,
    alta: false,
  });
  const [autores, setAutores] = useState([]);
  const [editoriales, setEditoriales] = useState([]);
  const [autor, setAutor] = useState();
  const [editorial, setEditorial] = useState();

  const obtenerListaAutores = async () => {
    await axios
      .get("http://localhost:8080/autores/autores/")
      .then(function (response) {
        setAutores(response.data);
        setAutor(response.data[0].id);
      });
  };
  const obtenerListaEditoriales = async () => {
    await axios
      .get("http://localhost:8080/editoriales/editoriales/")
      .then(function (response) {
        setEditoriales(response.data);
        setEditorial(response.data[0].id);
      });
  };

  useEffect(() => {
    obtenerListaAutores();
    obtenerListaEditoriales();
  }, []);

  const handleChange = (e) => {
    setLibro({
      ...libro,
      [e.target.name]: e.target.value,
    });
  };
  const handleChecked = async (e) => {
    setLibro({
      ...libro,
      [e.target.name]: e.target.checked,
    });
  };

  const handleChangeAutor = async (e) => {
    setAutor(parseInt(e.target.value));
  };
  const handleChangeEditorial = async (e) => {
    setEditorial(parseInt(e.target.value));
  };
  const onSubmit = (e) => {
    e.preventDefault();

    Swal.fire({
      title: "¿Deseas Crear el Libro?",
      showCancelButton: true,
      confirmButtonText: "Crear",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .post(
            `http://localhost:8080/libreria/crear/${autor}/${editorial}`,
            libro
          )
          .then((res) => {
            if (res.status == 201) {
              Swal.fire("Libro creado con exito!", "", "success");
              setLibro({
                isbn: null,
                titulo: "",
                anio: null,
                ejemplares: null,
                ejemplaresPrestados: null,
                ejemplaresRestantes: null,
                alta: false,
              });
              history.push("/listaLibros");
            }
          });
      } else if (result.isDenied) {
      }
    });
  };

  return (
    <Fragment>
      <div className="container form-crear-Libro">
        <form onSubmit={onSubmit} className="form-form-crear-Libro">
          <h1>Crear Libro</h1>
          <div class="mb-3">
            <label for="isbn" class="form-label">
              Isbn
            </label>
            <input
              type="number"
              class="form-control"
              id="isbn"
              name="isbn"
              aria-describedby="emailHelp"
              onChange={handleChange}
              //value={nombre}
            />
          </div>
          <div class="mb-3">
            <label for="titulo" class="form-label">
              Titulo del Libro
            </label>
            <input
              type="text"
              class="form-control"
              id="titulo"
              name="titulo"
              aria-describedby="emailHelp"
              onChange={handleChange}
              //value={nombre}
            />
          </div>
          <div class="mb-3">
            <label for="anio" class="form-label">
              Año
            </label>
            <input
              type="number"
              class="form-control"
              id="anio"
              name="anio"
              aria-describedby="emailHelp"
              onChange={handleChange}
              //value={nombre}
            />
          </div>
          <div class="mb-3">
            <label for="ejemplares" class="form-label">
              Ejemplares
            </label>
            <input
              type="number"
              class="form-control"
              id="ejemplares"
              name="ejemplares"
              aria-describedby="emailHelp"
              onChange={handleChange}
              //value={nombre}
            />
          </div>
          <div class="mb-3">
            <label for="ejemplaresPrestados" class="form-label">
              Ejemplares Prestados
            </label>
            <input
              type="number"
              class="form-control"
              id="ejemplaresPrestados"
              name="ejemplaresPrestados"
              aria-describedby="emailHelp"
              onChange={handleChange}
              //value={nombre}
            />
          </div>
          <div class="mb-3">
            <label for="ejemplaresRestantes" class="form-label">
              Ejemplares Restantes
            </label>
            <input
              type="number"
              class="form-control"
              id="ejemplaresRestantes"
              name="ejemplaresRestantes"
              aria-describedby="emailHelp"
              onChange={handleChange}
            />
          </div>

          <div class="mb-3 form-check">
            <input
              type="checkbox"
              class="form-check-input"
              id="alta"
              name="alta"
              onChange={handleChecked}
              //checked={alta}
            />
            <label class="form-check-label" for="alta">
              Alta
            </label>
          </div>

          <button type="submit" class="btn btn-primary">
            Crear
          </button>
        </form>
        <form className="form-select-leo">
          <div className="mb-3">
            <label>Seleccione Autor</label>
            <select
              class="form-select"
              aria-label="Default select example"
              onChange={handleChangeAutor}
            >
              {autores.map((autor) => (
                <option key={autor.id} value={autor.id}>
                  {autor.nombre}
                </option>
              ))}
            </select>
          </div>

          <div className="mb-3">
            <label>Seleccione Editorial</label>
            <select
              class="form-select"
              aria-label="Default select example"
              onChange={handleChangeEditorial}
            >
              {editoriales.map((editorial) => (
                <option key={editorial.id} value={editorial.id}>
                  {editorial.nombre}
                </option>
              ))}
            </select>
          </div>
        </form>
      </div>
    </Fragment>
  );
};

export default CrearLibro;
