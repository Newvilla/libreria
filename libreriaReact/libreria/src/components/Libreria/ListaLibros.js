import React, { Fragment, useState, useEffect } from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
const ListaLibros = () => {
  const axios = require("axios");
  const [libros, setLibros] = useState([]);
  const Swal = require("sweetalert2");

  const obtenerListaLibros = async () => {
    await axios
      .get("http://localhost:8080/libreria/libros")
      .then(function (response) {
        setLibros(response.data);
      });
  };
  const eliminarLibro = (libro) => {
    Swal.fire({
      title: "¿Seguro deseas eliminar este Libro?",
      icon: "warning",
      showCancelButton: true,
      confirmButtonColor: "#3085d6",
      cancelButtonColor: "#d33",
      confirmButtonText: "Si, Eliminar!",
    }).then((result) => {
      if (result.isConfirmed) {
        axios
          .delete(`http://localhost:8080/libreria/eliminar/${libro.id}`)
          .then(function (response) {});
        Swal.fire("Eliminado!", "Libro eliminado con exito.", "success");
        setLibros([]);
      }
    });
  };
  useEffect(() => {
    obtenerListaLibros();
  });
  return (
    <Fragment>
      <div className="container">
        <div className="header">
          <h1>Lista Libros</h1>
        </div>
        <div
          className="btn-toolbar"
          role="toolbar"
          aria-label="Toolbar with button groups"
        >
          <div class="btn-group" role="group" aria-label="Third group">
            <Link to="/crearLibro">
              <button className="btn btn-primary " type="button">
                Crear Libro
              </button>
            </Link>
          </div>
        </div>
        <table className="table">
          <thead className="table-dark">
            <tr>
              <th scope="col">ISBN</th>
              <th scope="col">Titulo</th>
              <th scope="col">Año</th>
              <th scope="col">Ejemplares</th>
              <th scope="col">Prestados</th>
              <th scope="col">Restantes</th>
              <th scope="col">Autor</th>
              <th scope="col">Editorial</th>
              <th scope="col">Alta</th>
              <th></th>
              <th></th>
            </tr>
          </thead>
          <tbody>
            {libros.map((libro) => (
              <tr>
                <td>{libro.isbn}</td>
                <td>{libro.titulo}</td>
                <td>{libro.anio}</td>
                <td>{libro.ejemplares}</td>
                <td>{libro.ejemplaresPrestados}</td>
                <td>{libro.ejemplaresRestantes}</td>
                <td>{libro.autor.nombre}</td>
                <td>{libro.editorial.nombre}</td>
                <td>{libro.alta ? "Alta" : "Baja"}</td>

                {/* <td>
                  <Link to={`/editarEditorial/${JSON.stringify(editorial)}`}>
                    <button type="button" className="btn btn-success">
                      Editar
                    </button>
                  </Link>
                </td> */}
                <td>
                  <button
                    type="button"
                    className="btn btn-danger"
                    onClick={() => eliminarLibro(libro)}
                  >
                    Eliminar
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </Fragment>
  );
};

export default ListaLibros;
