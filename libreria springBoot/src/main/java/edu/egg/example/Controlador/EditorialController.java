/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.egg.example.Controlador;

import edu.egg.example.Entidades.Editorial;
import edu.egg.example.Servicios.EditorialService;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author G Y L GROUP
 */
@Controller
@RequestMapping("/editoriales")
@CrossOrigin("*")
public class EditorialController {
    @Autowired
    EditorialService es;
    
    @RequestMapping(value = "/editoriales", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Editorial> listaEditoriales(){
    List<Editorial> editoriales = es.obtenerEditoriales();
    return editoriales;
    }
    
     @RequestMapping(value = "/crear", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity < String > crearAutor (@RequestBody Editorial editorial){
       es.crearEditorial(editorial);
         return ResponseEntity.status(HttpStatus.CREATED).build();
         
    }
    
    @RequestMapping(value = "/eliminar/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity < String > eliminarEditorial (@PathVariable(value="id") Integer editorialId){
       es.eliminaEditorial(editorialId);
         return ResponseEntity.status(HttpStatus.OK).build();
         
    }
    
      @RequestMapping(value = "/editar/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity < String > editarEditorial (@RequestBody Editorial editorialEdit,@PathVariable(value="id") Integer editorialId){
       
        Optional<Editorial> editorial = es.buscarPorId(editorialId);
        if(!editorial.isPresent()){
             return ResponseEntity.notFound().build();
        }
        es.editarEditorial(editorial, editorialEdit);
         return ResponseEntity.status(HttpStatus.OK).build();
         
    }
    
}
