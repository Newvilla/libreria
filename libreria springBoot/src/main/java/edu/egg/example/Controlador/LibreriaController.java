/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.egg.example.Controlador;

import edu.egg.example.Entidades.Libreria;
import edu.egg.example.Servicios.LibreriaService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author G Y L GROUP
 */
@Controller
@RequestMapping("/libreria")
@CrossOrigin("*")
public class LibreriaController {
    @Autowired
    LibreriaService ls;
    
    @RequestMapping(value = "/crear/{idAutor}/{idEditorial}", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity < String > crearLibro (@RequestBody Libreria libreria,@PathVariable(value="idAutor") Integer autorId,@PathVariable(value="idEditorial") Integer editorialId){
       ls.crearLibro(libreria,editorialId,autorId);
         return ResponseEntity.status(HttpStatus.CREATED).build();
         
    }
    
    @RequestMapping(value = "/libros", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public @ResponseBody List<Libreria> listaLibros(){
    List<Libreria> libros = ls.obtenerLibros();
    return libros;
    }
    
     @RequestMapping(value = "/eliminar/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity < String > eliminarLibro (@PathVariable(value="id") Integer libroId){
       ls.eliminarLibro(libroId);
         return ResponseEntity.status(HttpStatus.OK).build();
         
    }
}
