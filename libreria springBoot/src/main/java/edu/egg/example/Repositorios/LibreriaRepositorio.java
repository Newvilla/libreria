/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.egg.example.Repositorios;

import edu.egg.example.Entidades.Libreria;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 *
 * @author G Y L GROUP
 */
public interface LibreriaRepositorio extends JpaRepository<Libreria,Integer> {
    
}
