/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.egg.example.Servicios;

import edu.egg.example.Entidades.Autor;
import edu.egg.example.Entidades.Editorial;
import edu.egg.example.Entidades.Libreria;
import edu.egg.example.Repositorios.AutorRepositorio;
import edu.egg.example.Repositorios.EditorialRepositorio;
import edu.egg.example.Repositorios.LibreriaRepositorio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author G Y L GROUP
 */
@Service
public class LibreriaService {
      @Autowired
      LibreriaRepositorio lr;
       @Autowired
      EditorialService es;
        @Autowired
      AutorService as;
        @Transactional
    public Libreria crearLibro(Libreria libreriaSchema,Integer editorialId,Integer autorId){
        
        Libreria libro = new Libreria();
        
       Optional<Autor> autorOptional = as.buscarPorId(autorId);
       Optional<Editorial> editorialOptional = es.buscarPorId(editorialId);
       if(autorOptional.isPresent()&&editorialOptional.isPresent())
       {
         libro.setIsbn(libreriaSchema.getIsbn());
        libro.setTitulo(libreriaSchema.getTitulo());
        libro.setAnio(libreriaSchema.getAnio());
        libro.setEjemplares(libreriaSchema.getEjemplares());
        libro.setEjemplaresPrestados(libreriaSchema.getEjemplaresPrestados());
        libro.setEjemplaresRestantes(libreriaSchema.getEjemplaresRestantes());
        libro.setAlta(libreriaSchema.isAlta());
        libro.setAutor(autorOptional.get());
        libro.setEditorial(editorialOptional.get());
        lr.save(libro);
        return libro;
       
       }
      return null;
      
    }
    
     @Transactional
    public List<Libreria> obtenerLibros(){
        List<Libreria> libros = lr.findAll();
        return libros;
    }
     @Transactional
    public void eliminarLibro(Integer id){
        lr.deleteById(id);
        
    }
    
}
