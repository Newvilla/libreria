/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package edu.egg.example.Servicios;

import edu.egg.example.Entidades.Editorial;
import edu.egg.example.Repositorios.EditorialRepositorio;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author G Y L GROUP
 */
@Service
public class EditorialService {
    @Autowired
    EditorialRepositorio er;
    
    
    @Transactional
    public Editorial crearEditorial(Editorial editorialSchema){
        Editorial editorial = new Editorial();
        editorial.setNombre(editorialSchema.getNombre());
        editorial.setAlta(editorialSchema.isAlta());
        er.save(editorial);
        return editorial;
    }
    
     @Transactional
    public List<Editorial> obtenerEditoriales(){
        List<Editorial> editoriales = er.findAll();
        return editoriales;
    }
     @Transactional
    public void eliminaEditorial(Integer id){
        er.deleteById(id);
        
    }
      @Transactional
    public void editarEditorial(Optional<Editorial> editorial, Editorial editorialEdit){
       // BeanUtils.copyProperties(autorEdit,autor);
        editorial.get().setNombre(editorialEdit.getNombre());
        editorial.get().setAlta(editorialEdit.isAlta());
         er.save(editorial.get());
    }
    
     @Transactional
    public Optional<Editorial> buscarPorId(Integer id){
       return er.findById(id);
    }
}
